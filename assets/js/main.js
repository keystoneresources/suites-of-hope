$(document).ready(function(){
//Backstretch on homepage
	var indexClass = $('body').hasClass('page-1');
	if (indexClass === true) {
		$.backstretch('../assets/img/elements/bg_home.png');
	}

	//Colorboxes
	$('.colorbox_map').colorbox({inline:true, width:800});

//Scroll to top of page with the side tab
	$(window).scroll(function(){
		if($(this).scrollTop() > 100){
			$(".scrollup_tab").fadeIn();
		}else{
			$(".scrollup_tab").fadeOut();
		}
	});
	$('#scroll_to_top').click(function(e){
		e.preventDefault();
		$('body,html').animate({
			scrollTop: 0
		}, 1000);
		return false;
	});
//Local Scroll for section icons
	$.localScroll.hash({duration: 1500});
	$('#scroll').localScroll({
		duration: 1500,
		hash:true}
	);
//Equal Height Divs
	//$('.bio_box').equalHeights();
	$(".suite_info_area").equalHeights(100,500);
	$(".suite_faq .equal").equalHeights(100,700);
	$(".registry_items .equal").equalHeights(100,700);
	$(".home_box").equalHeights(100,500);
	$(".donor_box").equalHeights(100,1000);

//Suite Page Slideshow Code Start
	//This is added so all the suite slides don't flash on page load'

	if($('body.type-3').length){
  	$('ul#slide_links li a#image1').addClass('active');
  	//Hide selection arrows if a second Suite slide doesnt exist
  	if ($('ul#slide_links li a#image2').length){
          $('.suite_slideshow_arrow').show();
       }else{
      	$('.suite_slideshow_arrow').hide();
       }
  	//Jquery Cycle call for suites
      $('.suite_slideshow').cycle({
  		fx: 'scrollHorz',
  		timeout:  0,
  		after:  navChange,
  		speed: 200,
  		prev:   '#prev_suite',
     		next:   '#next_suite'
  	});
  	//This automatically changes the suite navigation link color based on the current slide
  	 var navLink = 'ul#slide_links li a'
          function navChange(curr, next, opts) {
              var suite = $('.suite_container' +( parseInt(opts.currSlide) + 1)).attr('data-slide');
              if(opts.currSlide === parseInt(opts.currSlide)){
                          console.log(opts.currSlide + "equals" + parseInt(opts.currSlide));
                          $(navLink).removeClass('active');
                          $(navLink + '.' + suite).addClass('active');
              }
          }
      //This changes the slide when you click a suite navigation link
      $(navLink).click(function() {
      $('.suite_slideshow').cycle(parseInt($(this).attr('data-link')) - 1);
      	return false;
   	 });
  //Suite Page Slideshow Code End
	}

//Stories of Hope, hide arrows if a 2nd story doesn't exist
	if ($('#storyA_2').length){
        $('.storyA_arrow').show();
     }else{
    	$('.storyA_arrow').hide();
     }
     if ($('#storyB_2').length){
        $('.storyB_arrow').show();
     }else{
    	$('.storyB_arrow').hide();
     }
     if ($('#storyC_2').length){
        $('.storyC_arrow').show();
     }else{
    	$('.storyC_arrow').hide();
     }
//Stories of Hope Jquery Cycles
	$('.story_slideshowA').cycle({
		fx: 'scrollHorz',
		timeout:  0,
		speed: 400,
		prev:   '#prev_story',
 	    next:   '#next_story'
	});
	$('.story_slideshowB').cycle({
		fx: 'scrollHorz',
		timeout:  0,
		speed: 400,
		prev:   '#prev_storyB',
   		next:   '#next_storyB'
	});
	$('.story_slideshowC').cycle({
		fx: 'scrollHorz',
		timeout:  0,
		speed: 400,
		prev:   '#prev_storyC',
    	next:   '#next_storyC'
	});

	if($('body.type-9').length || $('body.type-11').length){
	  $('.event_list_item:last').addClass("last");
	}

	if($('body.type-10').length){
  $('.event_slider').cycle({
  		fx: 'scrollHorz',
  		timeout:  0,
  		speed: 400,
  		prev: '#prev_event',
      next: '#next_event'
  	});
  	if($('.event_slider img').length <= 1){
    	$('.arrow_container a', this).hide();
  	}
  }//if type-10


//Contact Form Code
	$(window).hashchange(function(){
		var contactHash = window.location.hash;
		if (contactHash === '#patient-form'){
			if($('.contact_arrows').is(':visible')){
				$(".contact_arrows, .contact_header").animate({opacity:0},400, function(){
					$(this).hide();
					$("#form_2").hide();
					$("#form_1").show().css("opacity",0).animate({opacity:1}, 800);
				});
			}else{
				$("#form_2").animate({opacity:0}, 400, function(){
					$(this).hide();
					$("#form_1").show().css("opacity",0).animate({opacity:1}, 800);
				});
			}
		}else if(contactHash === '#donor-form'){
			if($('.contact_arrows').is(':visible')){
				$(".contact_arrows, .contact_header").animate({opacity:0},400, function(){
					$(this).hide();
					$("#form_1").hide();
					$("#form_2").show().css("opacity",0).animate({opacity:1}, 800);
				});
			}else{
				$("#form_1").animate({opacity:0}, 400, function(){
					$(this).hide();
					$("#form_2").show().css("opacity",0).animate({opacity:1}, 800);
				});
			}
		}else{
			$("#form_1, #form_2").hide();
			$(".contact_arrows, .contact_header").show().css("opacity",0).animate({opacity:1}, 800);
		}
	});
	$("#form_trigger_1").click(function(){
		window.location.hash = "#patient-form";
	});
	$("#form_trigger_2").click(function(){
		window.location.hash = "#donor-form";
	});
    $(window).trigger('hashchange');
//Form Validation
	$("#donors").validationEngine('attach');
	$("#patients").validationEngine('attach');

//Rounded Corner Polyfill/Shiv
	if (!Modernizr.borderradius) {
		$(".ie8_corner").corner();
	}

//Donation cart
if($('body.type-4').length){
  var diamond = $('.donate_medals .diamond_level').attr('data-level');
  var platinum = $('.donate_medals .platinum_level').attr('data-level');
  var gold = $('.donate_medals .gold_level').attr('data-level');
  $('.page-4 #donation_cart form:odd').find('.grid_4').addClass('push_3');

}


if($('body.page-41').length){
	
	$('input[type="submit"]').click(function(){
		$('.note').show();
	});

	$('input[checked="checked"]').next().addClass('selected'); 

	var saved2Amount = $('input#cart2q').val();
	var saved4Amount = $('input#cart4q').val();
	if(saved2Amount != '0' && saved2Amount != ''){
		console.log(saved2Amount);
		$('#pink-pigeons #cart2q').css('opacity', '1');
		$('.control-label[for="cart2q"]').css('opacity', '1');
		// $('input[value="2 Person Cart"]').next().addClass('selected'); 
	}else{
		$('input#cart2q').val('0');
	} 
	if(saved4Amount != '0' && saved4Amount != ''){
		console.log(saved4Amount);
		$('#pink-pigeons #cart4q').css('opacity', '1');
		$('.control-label[for="cart4q"]').css('opacity', '1');
		$('input[value="4 Person Cart"]').next().addClass('selected'); 
	}else{
		$('input#cart4q').val('0');
	} 

	$('.checkbox-custom').each(function(){
		$(this).click(function(){
			$(this).toggleClass('selected');
			var checkBoxes = $(this).prev();
      checkBoxes.prop("checked", !checkBoxes.prop("checked"));
      var boxValue = $(this).prev().val();
			var initial2Val = $('input#cart2q').val();
			var initial4Val = $('input#cart4q').val();
      if(boxValue == "2 Person Cart"){
	      if(initial2Val == 0){
		      $('input#cart2q').css('opacity', '1').val("1");
		      $('.control-label[for="cart2q"]').css('opacity', '1');
	      }else{
		      $('input#cart2q').css('opacity', '0').val('0');
		      $('.control-label[for="cart2q"]').css('opacity', '0');
	      }
      }
      if(boxValue == "4 Person Cart"){
	      if(initial4Val == 0){
		      $('input#cart4q').css('opacity', '1').val("1");
		      $('.control-label[for="cart4q"]').css('opacity', '1');
	      }else{
		      $('input#cart4q').css('opacity', '0').val("0");
		      $('.control-label[for="cart4q"]').css('opacity', '0');
	      }
      }
		});
	});
}// Page 41

if($('body.page-83').length){

	$('input[checked="checked"]').next().addClass('selected'); 
	
	$('input[type="submit"]').click(function(){
		$('.note').show();
	});

	var saved2Amount = $('input#tcart2q').val();
	var saved4Amount = $('input#tcart4q').val();
	if(saved2Amount != '0' && saved2Amount != ''){
		console.log(saved2Amount);
		$('#pink-pigeons #tcart2q').css('opacity', '1');
		$('.control-label[for="tcart2q"]').css('opacity', '1');
	}else{
		$('input#tcart2q').val('0');
	} 
	if(saved4Amount != '0' && saved4Amount != ''){
		console.log(saved4Amount);
		$('#pink-pigeons #tcart4q').css('opacity', '1');
		$('.control-label[for="tcart4q"]').css('opacity', '1');
		$('input[value="4 Person Cart"]').next().addClass('selected'); 
	}else{
		$('input#cart4q').val('0');
	} 

	$('.checkbox-custom').each(function(){
		$(this).click(function(){
			$(this).toggleClass('selected');
			var checkBoxes = $(this).prev();
      checkBoxes.prop("checked", !checkBoxes.prop("checked"));
      var boxValue = $(this).prev().val();
			var initial2Val = $('input#tcart2q').val();
			var initial4Val = $('input#tcart4q').val();
      if(boxValue == "2 Person Cart"){
	      if(initial2Val == 0){
		      $('input#tcart2q').css('opacity', '1').val("1");
		      $('.control-label[for="tcart2q"]').css('opacity', '1');
	      }else{
		      $('input#tcart2q').css('opacity', '0').val('0');
		      $('.control-label[for="tcart2q"]').css('opacity', '0');
	      }
      }
      if(boxValue == "4 Person Cart"){
	      if(initial4Val == 0){
		      $('input#tcart4q').css('opacity', '1').val("1");
		      $('.control-label[for="tcart4q"]').css('opacity', '1');
	      }else{
		      $('input#tcart4q').css('opacity', '0').val("0");
		      $('.control-label[for="tcart4q"]').css('opacity', '0');
	      }
      }
		});
	});
}// Page 83



if($('body.page-42').length){
	if($('#Individual').length){
		console.log("works");
		$('#Individual').submit();
	}
	if($('#Team').length){ 
		$('#Team').submit();
	}
}


//https://suitesofhope.foxycart.com/cart?
if($('body.page-4').length){

//clear out all form fields
fcc.events.cart.postprocess.add(function(e, arr) {
  $('#donation_cart form input[name="quantity"]').val('');
  console.log('postprocess.add executed');

  /*
jQuery.getJSON('https://suitesofhope.foxycart.com/cart?'+fcc.session_get()+'&output=json&name=test_product&price=5&quantity=1&callback=?', function(cart) {
  	console.log('cart updated');
  });
*/

		//		$.getJSON('http://suitesofhope.foxycart.com/cart?'+fcc.session_get()+'&output=json&cart=update&id=42007721&quantity=50&callback=?', function(){});


  //console.log(FC);
	return true;
});
}

if($('body.type-9').length){
  if(!$('.event_list_item').length){
    $('.no_events').show();
  }else{
    $('.no_events').hide();
  }
}







}); // close document.ready()