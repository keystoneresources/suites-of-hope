<?php
  require_once '/paas/c1283/www/config.core.php';
  require_once MODX_CORE_PATH.'model/modx/modx.class.php';
  $modx = new modX();
  $modx->initialize('web');
  $modx->getService('error','error.modError', '', '');

  //$modx->log(modX::LOG_LEVEL_ERROR,'parseFoxycartXML Script Hit');

  if(isset($_POST["FoxyData"])){
    
    // Set FoxyCart API Key
    $myKey = 'wW2UNv4WFbKgNC5tSlL6LkfGCUFdZIllvh6eC41daFjNdi8VuNVbY5LvQlyJ';
    
    // Connect to DB  	
  	$link = mysqli_connect('localhost', 'c1283', 'YxaDxkyLU0Dk', 'instance_c1283_modx'); //live
    if(mysqli_connect_errno()){
      $modx->log(modX::LOG_LEVEL_ERROR, 'Could not connect: '.mysqli_connect_error());
      return false;
    }
  	
  	// Get the raw data and initialize variables
  	$FoxyData_encrypted = urldecode($_POST["FoxyData"]);
  	$FoxyData_decrypted = rc4crypt::decrypt($myKey,$FoxyData_encrypted);
  	$xml = new SimpleXMLElement($FoxyData_decrypted);
  	
  	foreach($xml->transactions->transaction as $transaction){
  		
  		//set default success message.
  		$success = "foxy";
  		
  		// Get the transaction ID
  		$order_id = $transaction->id;
  		
  		// Get the custom fields
  		foreach ($transaction->custom_fields->custom_field as $field) {
				if($field->custom_field_name == 'Recognition'){ $recognition = $field->custom_field_value; }
				if($field->custom_field_name == 'Display_Recognition'){ $display_recognition = $field->custom_field_value; }
				if($field->custom_field_name == 'Recognition_Name'){ $recognition_display_name = $field->custom_field_value; }
				if($field->custom_field_name == 'Display_Contributor'){ $display_contributor = $field->custom_field_value; }
				if($field->custom_field_name == 'Contributor_Name'){ $contributor_display_name = $field->custom_field_value; }
			}
  		
/*
  		//Log variables for testing
  		$modx->log(modX::LOG_LEVEL_ERROR,'$order_id: '.$order_id);
  		$modx->log(modX::LOG_LEVEL_ERROR,'$recognition: '.$recognition);
  		$modx->log(modX::LOG_LEVEL_ERROR,'$display_recognition: '.$display_recognition);
  		$modx->log(modX::LOG_LEVEL_ERROR,'$recognition_display_name: '.$recognition_display_name);
  		$modx->log(modX::LOG_LEVEL_ERROR,'$display_contributor: '.$display_contributor);
  		$modx->log(modX::LOG_LEVEL_ERROR,'$contributor_display_name: '.$contributor_display_name);
*/
  		
  		//Make the DB query
  		$sql = "INSERT INTO `foxycart_donation_info` (
  		          order_id, 
  		          recognition, 
  		          display_recognition, 
  		          recognition_display_name, 
  		          display_contributor, 
  		          contributor_display_name,
  		          product_total
  		        ) VALUES (
  		          $order_id,
  		          '$recognition',
  		          '$display_recognition',
  		          '$recognition_display_name',
  		          '$display_contributor',
  		          '$contributor_display_name',
  		          '$product_total'
  		        )";
  		$result = mysqli_query($link, $sql);
  		if(!$result){
        $success = "error";
        $modx->log(modX::LOG_LEVEL_ERROR,'Foxycart XML Datafeed Error: No $result from foxycart_donation_info');
        $modx->log(modX::LOG_LEVEL_ERROR, 'Error saving to foxycart_donation_info: '.$sql.' Error: '.mysqli_error($link));
      }
      
      //Save campaign donation info
      foreach ($transaction->transaction_details->transaction_detail as $product) {
        $product_name = $product->product_name;
        
        //check to see if a campaign donation product was purchased
        if($product_name == 'Campaign Donation'){
          
          //set variables
          $product_price = $product->product_price;
          $product_quantity = $product->product_quantity;
          $product_total = floatval($product_price) * floatval($product_quantity);
          //get product options
          foreach($product->transaction_detail_options->transaction_detail_option as $product_option){
            if($product_option->product_option_name == 'userid'){ $user_id = $product_option->product_option_value; }
    				if($product_option->product_option_name == 'User_Name'){ $user_name = $product_option->product_option_value; }
          }
          
/*
          $modx->log(modX::LOG_LEVEL_ERROR,'$product_name: '.$product_name);
          $modx->log(modX::LOG_LEVEL_ERROR,'$user_id: '.$user_id);
          $modx->log(modX::LOG_LEVEL_ERROR,'$user_name: '.$user_name);
          $modx->log(modX::LOG_LEVEL_ERROR,'$product_price: '.$product_price);
          $modx->log(modX::LOG_LEVEL_ERROR,'$product_quantity: '.$product_quantity);
          $modx->log(modX::LOG_LEVEL_ERROR,'$product_total: '.$product_total);
*/
          
          //Make the DB query
      		$sql_cd = "INSERT INTO `foxycart_donation_campaign` (
      		          order_id,
      		          user_id,
      		          user_name,
      		          product_price,
      		          product_quantity,
      		          product_total
      		        ) VALUES (
      		          $order_id,
      		          '$user_id',
      		          '$user_name',
      		          '$product_price',
      		          '$product_quantity',
      		          '$product_total'
      		        )";
      		$result_cd = mysqli_query($link, $sql_cd);
      		if(!$result_cd){
            $success = "error";
            $modx->log(modX::LOG_LEVEL_ERROR,'Foxycart XML Datafeed Error: No $result from foxycart_donation_info');
            $modx->log(modX::LOG_LEVEL_ERROR, 'Error saving to foxycart_donation_info: '.$sql_cd.' Error: '.mysqli_error($link));
          }
          
        }//end if campaign donation purchased
      }//end $product loop for campaign donation

  		// Return value of $success to Foxycart
  		mysqli_close($link);
  		die($success);
  	}
  }else{
    $modx->log(modX::LOG_LEVEL_ERROR,'Foxycart XML Datafeed Error: No FoxyData sent to script');
    die("error");
  }//end if data sent

  // ======================================================================================
  // RC4 ENCRYPTION CLASS
  // Do not modify.
  // ======================================================================================
  class rc4crypt {
  	function encrypt ($pwd, $data, $ispwdHex = 0){
  		if ($ispwdHex)
  			$pwd = @pack('H*', $pwd); // valid input, please!
   
  		$key[] = '';
  		$box[] = '';
  		$cipher = '';
   
  		$pwd_length = strlen($pwd);
  		$data_length = strlen($data);
   
  		for ($i = 0; $i < 256; $i++){
  			$key[$i] = ord($pwd[$i % $pwd_length]);
  			$box[$i] = $i;
  		}
  		for ($j = $i = 0; $i < 256; $i++){
  			$j = ($j + $box[$i] + $key[$i]) % 256;
  			$tmp = $box[$i];
  			$box[$i] = $box[$j];
  			$box[$j] = $tmp;
  		}
  		for ($a = $j = $i = 0; $i < $data_length; $i++){
  			$a = ($a + 1) % 256;
  			$j = ($j + $box[$a]) % 256;
  			$tmp = $box[$a];
  			$box[$a] = $box[$j];
  			$box[$j] = $tmp;
  			$k = $box[(($box[$a] + $box[$j]) % 256)];
  			$cipher .= chr(ord($data[$i]) ^ $k);
  		}
  		return $cipher;
  	}
  	function decrypt ($pwd, $data, $ispwdHex = 0){
  		return rc4crypt::encrypt($pwd, $data, $ispwdHex);
  	}
  }
?>